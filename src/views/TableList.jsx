/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";

import TableList from "components/TableList/AdminTable.jsx";
class TableAdmin extends Component {

  constructor(props) {
    super(props);
    this.state={
      list:[],
      headers:["Head1","Head2"]
    };
  }
  

  render() {
    return (
      <TableList title="This is title" subtitle="Subtitle" list={this.state.list} headers={this.state.headers}/>
    );
  }
}

export default TableAdmin;
