import React, { Component } from "react";
import { Grid, Row, Col, Table, Pagination } from "react-bootstrap";
import "../../style.css";
import Card from "components/Card/Card.jsx";
import { thArray, tdArray, tdArray2 } from "variables/Variables.jsx";
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import filterFactory, { multiSelectFilter, textFilter, numberFilter, Comparator } from 'react-bootstrap-table2-filter';
import cellEditFactory, { Type } from 'react-bootstrap-table2-editor';
import SweetAlert from 'sweetalert2-react';
// import { Filters, FilterBar } from 'react-dynamic-filterbar';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import overlayFactory from 'react-bootstrap-table2-overlay';
class TableList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: props.title,
            subtitle: props.subtitle,
            list: props.list ? props.list : tdArray,
            pagenumber: props.pagenumber ? props.pagenumber : 1,
            totalpage: props.totalpage ? props.totalpage : 1,
            pagesize: props.pagesize ? props.pagesize : 10,
            isplain: props.plain ? props.plain : false,
            pagingsize: props.pagingsize ? props.pagingsize : 10,
            pagingstartnumb: 1,
            pagingendnumb: 1,
            tableHeaders: props.headers ? props.headers : thArray,
            fql: undefined,
        }
    }

    render() {
        const selectOptions = {
            0: 'good',
            1: 'Bad',
            2: 'unknown'
        };
        const cellEdit = cellEditFactory({
            mode: 'dbclick'

        });
        const compareCondition = (value) => {

        }
        const columns = [{
            dataField: 'id',
            text: 'ID',
            headerStyle: { outline: "none" },
            filter: textFilter({
                disabled: "disabled",
                placeholder: ""
            }),footer: 'Footer 1',
            sort:true
        }, {
            dataField: 'name',
            text: 'Name',
            headerStyle: { outline: "none" },
            
            filter: textFilter({
                placeholder: 'My Custom PlaceHolder',  // custom the input placeholder
                className: 'input', // custom classname on input
                defaultValue: 'test', // default filtering value
                comparator: Comparator.LIKE, // default is Comparator.LIKE
                caseSensitive: true, // default is false, and true will only work when comparator is LIKE
                delay: 1000, // how long will trigger filtering after user typing, default is 500 ms
                onClick: e => console.log(e),
                getFilter: (filter) => { // nameFilter was assigned once the component has been mounted.
                },
                onFilter: (filterValue) => {
                    //...
                }
            }),
            editor: {

            },footer: 'Footer 1',
            sort:true
        }, {
            dataField: 'salary',
            text: 'salary',
            headerStyle: { outline: "none" },
            filter: numberFilter({
                style: { display: 'inline-flex' }, // custom the style on number filter
                comparatorStyle: { minWidth: '65px' }, // custom the style on comparator select
                onFilter: (filterValue) => {
                    const regex = /\d+[/,,/.]*\d+/g;
                    return tdArray2.filter((x) => {
                        const withoutPrefixNumber = Number(x.salary.match(regex)[0].replace(',', ''))
                        filterValue.number = Number(filterValue.number)
                        switch (filterValue.comparator) {
                            case '>':
                                return withoutPrefixNumber > filterValue.number
                            case '>=':
                                return withoutPrefixNumber >= filterValue.number
                            case '<':
                                return withoutPrefixNumber < filterValue.number
                            case '<=':
                                return withoutPrefixNumber <= filterValue.number
                            case '=':
                                return withoutPrefixNumber === filterValue.number
                            case '!=':
                                return withoutPrefixNumber !== filterValue.number
                            default:
                                return true;
                        }
                    }
                    )

                    // return tdArray2.where('salary','>','50000')
                },
                getFilter: (filter) => {
                    console.log(filter, " filter ne")

                }
            }),
            sort:true
        }, {
            dataField: 'country',
            text: 'country',
            headerStyle: { outline: "none" },
            filter: textFilter(),
            sort:true
        }, {
            dataField: 'city',
            text: 'city',
            headerStyle: { outline: "none" },
            filter: textFilter(),
            sort:true
        }];
        const handleOnSelect = (row, isSelect) => {
            if (isSelect) {
                this.setState(() => ({
                    selected: [...this.state.selected, row.id]
                }));
            } else {
                this.setState(() => ({
                    selected: this.state.selected.filter(x => x !== row.id)
                }));
            }
        };

        const handleOnSelectAll = (isSelect, rows) => {
            const ids = rows.map(r => r.id);
            if (isSelect) {
                this.setState(() => ({
                    selected: ids
                }));
            } else {
                this.setState(() => ({
                    selected: []
                }));
            }
        };
        const selectRow = {
            mode: 'checkbox',
            clickToSelect: true,
            clickToEdit: true,
            //clickToSelectAndEditCell: true,
            selected: this.state.selected,
            onSelect: this.handleOnSelect,
            onSelectAll: this.handleOnSelectAll
        };
        paginationFactory({
            page: 1, // Specify the current page. It's necessary when remote is enabled
            sizePerPage: 7, // Specify the size per page. It's necessary when remote is enabled
            totalSize: 100, // Total data size. It's necessary when remote is enabled
            pageStartIndex: 0, // first page will be 0, default is 1
            paginationSize: 3,  // the pagination bar size, default is 5
            showTotal: true, // display pagination information
            sizePerPageList: [{
                text: '5', value: 5
            }, {
                text: '10', value: 10
            }, {
                text: 'All', value: tdArray2.length
            }], // A numeric array is also available: [5, 10]. the purpose of above example is custom the text
            withFirstAndLast: false, // hide the going to first and last page button
            alwaysShowAllBtns: true, // always show the next and previous page button
            firstPageText: 'First', // the text of first page button
            prePageText: 'Prev', // the text of previous page button
            nextPageText: 'Next', // the text of next page button
            lastPageText: 'Last', // the text of last page button
            nextPageTitle: 'Go to next', // the title of next page button
            prePageTitle: 'Go to previous', // the title of previous page button
            firstPageTitle: 'Go to first', // the title of first page button
            lastPageTitle: 'Go to last', // the title of last page button
            hideSizePerPage: true, // hide the size per page dropdown
            hidePageListOnlyOnePage: true, // hide pagination bar when only one page, default is false
            onPageChange: () => { }, // callback function when page was changing
            onSizePerPageChange: (sizePerPage, page) => { }, // callback function when page size was changing
            paginationTotalRenderer: (from, to, size) => { }  // custom the pagination total
        });
        const {SearchBar}=Search
        
        const beforeFilter=(newResult, newFilters)=> {
            console.log(newResult);
            console.log(newFilters);
          }
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        {/* <Col md={12} >
                            <Card {...this.props.plain === true ? "plain" : ""}
                                title={this.props.title != null ? this.props.title : "Table with Hover"}
                                category={this.props.subtitle != null ? this.props.subtitle : "Here is a subtitle for this table"}
                                ctTableFullWidth
                                ctTableResponsive
                                content={
                                    <Table striped hover>
                                        <thead>
                                            <tr>
                                                {this.state.tableHeaders.map((prop, key) => {
                                                    return <th key={key}>{prop}</th>;
                                                })}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.list.map((prop, key) => {
                                                return (
                                                    <tr key={key} className="clickable-row" itemID="">
                                                        {prop.map((prop, key) => {
                                                            return <td key={key}>{prop}</td>;
                                                        })}
                                                    </tr>
                                                );
                                            })}
                                        </tbody>

                                    </Table>
                                }
                            />
                        </Col> */}
                        {/* <FilterBar  buttonClassName="btn">
                            <Filters.StringFilter field={['firstName', 'lastName']} label="Name" className="form-control" buttonClassName="btn btn-primary" />
                            <Filters.StringFilter field="comment" label="Comment" className="form-control" buttonClassName="btn btn-primary" showOperator />
                            <Filters.NumericFilter field="amount" label="Amount" className="form-control" />
                            <Filters.SelectFilter field="color" label="Colors" options={selectOptions}  isMulti />
                            <Filters.DateFilter field="birthday" label="Birthday" showOperator buttonClassName="btn btn-primary" shown />
                        </FilterBar>); */}
                        <Col md={12} className="text-center">
                            {/* <Paging>

                            </Paging> */}



                            {/* <ToolkitProvider
                                keyField="id"
                                data={tdArray2}
                                columns={columns}
                                search
                            >
                                {
                                    props => (
                                        <div>
                                            <h3>Input something at below input field:</h3>
                                            <SearchBar {...props.searchProps} />
                                            <hr />
                                            <BootstrapTable
                                                {...props.baseProps}
                                            />
                                        </div>
                                    )
                                }
                            </ToolkitProvider> */}

                        </Col>
                        <Col md={12}>
                            <Card {...this.props.plain === true ? "plain" : ""}
                                title={this.props.title != null ? this.props.title : "Table with Hover"}
                                category={this.props.subtitle != null ? this.props.subtitle : "Here is a subtitle for this table"}
                                content={<SearchBar  />}>
                                
                            </Card>
                     
                            <BootstrapTable wrapperClasses="table-responsive"  striped hover bordered={false} insertRow={true} cellEdit={cellEdit} keyField='id' data={tdArray2, tdArray2}
                                columns={columns} filter={filterFactory({ beforeFilter })} pagination={paginationFactory()} selectRow={selectRow}
                                sort={{ dataField: 'salary', order: 'asc' }} noDataIndication="There is no records to present"
                                loading={ true }  //only loading is true, react-bootstrap-table will render overlay
                                
                            >

                            </BootstrapTable>


                        </Col>
                        {/* <Col md={12}>
                            <Card
                                plain
                                title={this.props.title != null ? this.props.title : "Table with Hover"}
                                category={this.props.subtitle != null ? this.props.subtitle : "Here is a subtitle for this table"}
                                ctTableFullWidth
                                ctTableResponsive
                                content={
                                    <Table hover>
                                        <thead>
                                            <tr>
                                                {thArray.map((prop, key) => {
                                                    return <th key={key}>{prop}</th>;
                                                })}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {tdArray.map((prop, key) => {
                                                return (
                                                    <tr key={key}>
                                                        {prop.map((prop, key) => {
                                                            return <td key={key}>{prop}</td>;
                                                        })}
                                                    </tr>
                                                );
                                            })}
                                        </tbody>
                                    </Table>
                                }
                            />
                        </Col> */}
                    </Row>
                </Grid>
            </div>
        );
    }
}
export default TableList;
