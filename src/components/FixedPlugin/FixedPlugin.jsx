/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React, { Component } from "react";
import Toggle from "react-toggle";
import "../../style.css";
import imagine1 from "assets/img/sidebar-1.jpg";
import imagine2 from "assets/img/sidebar-2.jpg";
import imagine3 from "assets/img/sidebar-3.jpg";
import imagine4 from "assets/img/sidebar-4.jpg";
import Draggable from 'react-draggable';
import ReactDOM from 'react-dom'
class FixedPlugin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: "dropdown show-dropdown open",
      bg_checked: true,
      bgImage: this.props.bgImage,
      isDragging: false,

      left: this.props.left,
      top: this.props.top,
      bottom: this.props.bottom,
      position: null
    };
    this.updateBounds = this.updateBounds
  }
  componentDidMount() {
    this.updateBounds(null, null)
    window.onresize = () => {
      this.updateBounds(null, null)
      this.setState({ position: { x: 0, y: 0 } })
    }
    console.log('mount')
  }
  componentDidUpdate() {

  }
  onChangeClick = () => {
    this.props.handleHasImage(!this.state.bg_checked);
    this.setState({ bg_checked: !this.state.bg_checked });
  };
  handleDrag = () => {
    this.setState({ isDragging: true })
  }
  handleClick = () => {
    if (!this.state.isDragging)
      this.props.handleFixedClick();

  };
  updateBounds = (event, data) => {
    if (data === null)
      return
    // Viewport (wrapper)
    const plugin = document.getElementById('fixed-plugin')
    const mainPanel = document.getElementById('main-panel')
    const sidebar = document.getElementById('sidebar')
    const dropmenu = document.getElementById("dropdown-menu")
    const node = ReactDOM.findDOMNode(this.$el);
    const pluginbound = node.getBoundingClientRect()
    const documentElement = document.documentElement
    const wrapperHeight = -this.state.top //(window.innerHeight || documentElement.clientHeight)
    const wrapperWidth = -this.state.left //(window.innerWidth || documentElement.clientWidth)

    // Draggable element center coordinates (x,y)
    // Here we assume that the Draggable Button (from the question)
    // is a rectangle. But we can easily change it with whatever 
    // figure we want and fine-tune the calculation.
    // Credits: https://stackoverflow.com/a/18932029/4312466
    const center = {
      x: data.x - (data.node.clientWidth / 2),
      y: data.y - (data.node.clientHeight / 2)
    }

    // The margin from the draggable's center,
    // to the viewport sides (top, left, bottom, right)
    const margin = {
      //top: wrapperHeight - center.y,
      left: wrapperWidth - center.x,
      //bottom: center.y - 0,
      right: center.x - 0
    }

    // When we get the nearest viewport side (below), then we can 
    // use these metrics to calculate the new draggable sticky `position`
    const position = {
      //top: { y: -(wrapperHeight - data.node.clientHeight), x: data.x },
      left: { y: data.y, x: -(wrapperWidth - data.node.clientWidth) },
      //bottom: { y: 0, x: -data.x },
      right: { y: data.y, x: 0 }
    }

    // Knowing the draggable's margins to the viewport sides,
    // now we can sort them out and get the smaller one.
    // The smallest margin defines the nearest viewport side to draggable.
    const sorted = Object.keys(margin).sort((a, b) => {
      console.log("a: ", a, " b: ", b, " margin: ", margin[a], margin[b])
      return margin[a] + margin[b]
    })
    const nearestSide = sorted[0]
    console.log(position[nearestSide], data, center, position, nearestSide, sorted, margin, dropmenu.clientHeight)
    this.setState({ position: position[nearestSide] })
    this.setState({

      left: -(mainPanel.clientWidth - plugin.clientWidth - (window.clientWidth<=990 ? 200 : 40)),
      top: window.innerWidth <= 990 ? -40 : -130,

    })
    // if (position[nearestSide].x <= 50) {
    //   const regex = /scale\(.+\)/g
    //   if (regex.test(plugin.style.transform))
    //     plugin.style.transform = plugin.style.transform.replace(regex, "scale(-1,1)")
    //   else
    //     plugin.style.transform += "scale(-1,1)"
    //   dropmenu.style.transform = "scale(-1,1)"
    // }
    // else
    //   plugin.style.transform = plugin.style.transform.replace(regex, "scale(1,1)")
    // if (document.getElementById("fixed-plugin").classList.contains("show open") === true)

    // console.log(plugin, dropmenu, this.$el, node, pluginbound, window.innerWidth - sidebar.clientWidth - 30, this.props.isNavOpen)
  }
  handleStop = (e, data) => {
    this.updateBounds(e, data)
    setTimeout(
      () => this.setState({ isDragging: false }),
      100
    );
  }

  render() {
    return (
      <Draggable
        axis="both"
        bounds={{
          left: this.state.left,
          top: this.state.top,
          right: 0, bottom: this.state.bottom
        }}
        allowAnyClick={false}
        position={this.state.position}
        onDrag={this.handleDrag}
        onStop={this.handleStop}
        grid={[20, 20]}>
        <div className="fixed-plugin" id="fixed-plugin" ref={el => {
          if (el)
            this.$el = el
        }} style={{ transform: "scale(1,1)" }}>
          <div id="fixedPluginClasses" className={this.props.fixedClasses}>
            <div onClick={this.handleClick}>
              <i className="fa fa-cog fa-2x" />
            </div>
            <ul className="dropdown-menu" id="dropdown-menu">
              <li className="header-title">Configuration</li>
              <li className="adjustments-line">
                <p className="pull-left">Background Image</p>
                <div className="pull-right">
                  <Toggle
                    defaultChecked={this.state.bg_checked}
                    onChange={this.onChangeClick}
                  />
                </div>
                <div className="clearfix" />
              </li>
              <li className="adjustments-line">
                <a className="switch-trigger">
                  <p>Filters</p>
                  <div className="pull-right">
                    <span
                      className={
                        this.props.bgColor === "black"
                          ? "badge filter active"
                          : "badge filter"
                      }
                      data-color="black"
                      onClick={() => {
                        this.props.handleColorClick("black");
                      }}
                    />
                    <span
                      className={
                        this.props.bgColor === "azure"
                          ? "badge filter badge-azure active"
                          : "badge filter badge-azure"
                      }
                      data-color="azure"
                      onClick={() => {
                        this.props.handleColorClick("azure");
                      }}
                    />
                    <span
                      className={
                        this.props.bgColor === "green"
                          ? "badge filter badge-green active"
                          : "badge filter badge-green"
                      }
                      data-color="green"
                      onClick={() => {
                        this.props.handleColorClick("green");
                      }}
                    />
                    <span
                      className={
                        this.props.bgColor === "orange"
                          ? "badge filter badge-orange active"
                          : "badge filter badge-orange"
                      }
                      data-color="orange"
                      onClick={() => {
                        this.props.handleColorClick("orange");
                      }}
                    />
                    <span
                      className={
                        this.props.bgColor === "red"
                          ? "badge filter badge-red active"
                          : "badge filter badge-red"
                      }
                      data-color="red"
                      onClick={() => {
                        this.props.handleColorClick("red");
                      }}
                    />
                    <span
                      className={
                        this.props.bgColor === "purple"
                          ? "badge filter badge-purple active"
                          : "badge filter badge-purple"
                      }
                      data-color="purple"
                      onClick={() => {
                        this.props.handleColorClick("purple");
                      }}
                    />
                  </div>
                  <div className="clearfix" />
                </a>
              </li>
              <li className="header-title">Sidebar Images</li>
              <li className={this.state["bgImage"] === imagine1 ? "active" : ""}>
                <a
                  className="img-holder switch-trigger"
                  onClick={() => {
                    this.setState({ bgImage: imagine1 });
                    this.props.handleImageClick(imagine1);
                  }}
                >
                  <img src={imagine1} alt="..." />
                </a>
              </li>
              <li className={this.state["bgImage"] === imagine2 ? "active" : ""}>
                <a
                  className="img-holder switch-trigger"
                  onClick={() => {
                    this.setState({ bgImage: imagine2 });
                    this.props.handleImageClick(imagine2);
                  }}
                >
                  <img src={imagine2} alt="..." />
                </a>
              </li>
              <li className={this.state["bgImage"] === imagine3 ? "active" : ""}>
                <a
                  className="img-holder switch-trigger"
                  onClick={() => {
                    this.setState({ bgImage: imagine3 });
                    this.props.handleImageClick(imagine3);
                  }}
                >
                  <img src={imagine3} alt="..." />
                </a>
              </li>
              <li className={this.state["bgImage"] === imagine4 ? "active" : ""}>
                <a
                  className="img-holder switch-trigger"
                  onClick={() => {
                    this.setState({ bgImage: imagine4 });
                    this.props.handleImageClick(imagine4);
                  }}
                >
                  <img src={imagine4} alt="..." />
                </a>
              </li>

              <li className="button-container">
                <div className="">
                  <a
                    href="https://www.creative-tim.com/product/light-bootstrap-dashboard-react?ref=lbdr-fixed-plugin"
                    target="_blank"
                    className="btn btn-success btn-block btn-fill"
                  >
                    Download free!
                </a>
                </div>
              </li>
              <li className="button-container">
                <div className="">
                  <a
                    href="https://www.creative-tim.com/product/light-bootstrap-dashboard-pro-react?ref=lbdr-fixed-plugin"
                    target="_blank"
                    className="btn btn-warning btn-block btn-fill"
                  >
                    Buy Pro
                </a>
                </div>
              </li>
              <li className="button-container">
                <a
                  href="https://demos.creative-tim.com/light-bootstrap-dashboard-react/#/documentation/getting-started?ref=lbdr-fixed-plugin"
                  target="_blank"
                  className="btn btn-fill btn-info"
                >
                  Documentation
              </a>
              </li>
            </ul>
          </div>
        </div>
      </Draggable>
    );
  }
}

export default FixedPlugin;
